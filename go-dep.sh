#!/bin/sh

docker run --rm --volumes-from app-gopath -v $PWD:/go/src/app golang:alpine /bin/sh -c "apk add --no-cache git && go get -u github.com/golang/dep/cmd/dep && cd /go/src/app && dep ${1}"