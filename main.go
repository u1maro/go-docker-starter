
package main

import (
	"fmt"
	"github.com/kpango/glg"
	"github.com/uniplaces/carbon"
)

func main() {
	fmt.Println("Hello go World")

	now, err := carbon.NowInLocation("Asia/Tokyo")
	if err != nil {
		glg.Error("cannot determine datetime")

		return
	}

	glg.Infof("Right now is %s\n", now.DateTimeString())
}
